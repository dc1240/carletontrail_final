/**
 * Main.java
 *
 * Created by Camille Benson, Dane Christensen, Chielon S. Yang
 *
 * A main class that sets up the stage and scene for the CarletonTrail game.
 */

package CarletonTrail;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {

    @Override
    public void start(final Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("CarletonTrail.fxml"));
        Parent root = (Parent)loader.load();

        //get an instance of the controller so we can update the information on the main screen after a game.
        final Controller controller = (Controller) loader.getController();
        controller.setControllerInstance(controller);
        primaryStage.setTitle("Carleton Trail");
        primaryStage.setScene(new Scene(root, 800, 550));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}

