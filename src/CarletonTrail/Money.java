/**
 * Money.java
 *
 * Created by Camille Benson, Dane Christensen, Chielon S. Yang
 *
 * Model for the money object that stores its position and provides a step
 * function for moving it across the screen.
 */

package CarletonTrail;

import javafx.fxml.FXML;
import javafx.scene.shape.Rectangle;


public class Money extends Rectangle{
    @FXML private double xPosition;
    @FXML private double yPosition;
    @FXML private double velocity;

    public Money() {
    }

    public void setXPosition(double newPosition) {
        this.xPosition = newPosition;
        this.setLayoutX(this.xPosition);
    }

    public void setYPosition(double newPosition) {
        this.yPosition = newPosition;
        this.setLayoutX(this.yPosition);
    }

    public void setVelocity(double newVelocity) {
        this.velocity = newVelocity;
    }

    public double getXPosition() {
        return xPosition;
    }

    public double getYPosition() {
        return yPosition;
    }

    public double getVelocity() {
        return this.velocity;
    }

    public void step() {
        this.yPosition = this.yPosition + this.velocity;
        this.setLayoutY(this.yPosition);
    }
}