package CarletonTrail;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * CarletonTrail.SleepGameController
 * Created by christed, bensonc, and yangch on 11/21/14.
 * The sleep game Controller is responsible for running the SleepGame.
 * It is a spinner based game where the player must stop on the desired hours of sleep.
 */
public class SleepGameController extends Controller {
    private AnchorPane rootToReturnTo;
    private CarletonTrailModel model;
    private Controller mainController;
    public int HoursOfSleep;
    public Text sleepGameText;
    public Text sleepGameDescription;
    public SleepGame game;
    public ArrayList<Integer> sleepList;
    private Timer timer;
    private double FRAMES_PER_SECOND = 1.0;
    private int sleepIndex;
    private boolean startPushed;

    /*
     * The constructor calls sleep game which generates a random list of integers from 0-9
     * and set it as the ArrayList sleepList.
     */
    public SleepGameController() {
        game = new SleepGame();
        sleepList = game.getSleepInts();

    }

    /*
     * initialize. This is responsible for setting the labels for energy, happiness, and money as well as the base
     * values for sleepIndex, start pushed, and the in game description. sleep index will be used by the update anamation
     * function in keeping track of the loop and when it needs to be reset. startPushed is a boolean, which makes it
     * so only one timer will be started when the Start button is pushed.
     */
    public void initialize() {
        HoursOfSleep = 0;
        this.energyLabel.setText(String.format("Energy:"));
        this.happinessLabel.setText(String.format("Happiness:"));
        this.moneyLabel.setText(String.format("Money:"));
        sleepIndex = 0;
        startPushed = false;
        sleepGameDescription.setText("SleepGame. The purpose of this game is to allow you recover energy and " +
                " happiness. \n\n How to play: Push Start to begin cycling through numbers 0-9,\n when you your desired amount of sleep push Stop. \n" +
                " The Goal is to stop it on the highest number possible. \n"  +
                " However, be careful because you only get one try.");

    }

    /*
     * this method allows us to pass in an instance of the main page to return to when sleepGame is done
     */
    public void setRootToReturnTo(AnchorPane rootToReturnTo) {
        this.rootToReturnTo = rootToReturnTo;
    }

    /*
     * this give SleepGame an instance of the Model so we can have access to all of the current parameters.
     */
    public void setModel(CarletonTrailModel model) {
        this.model = model;
    }

    /*
   * setController. setController allows us to pass in an instance of the main Controller. By doing this we are able to update
   * the values on the CarletonTrail.fxml file after the game is complete.
   */
    public void setController(Controller variable) { this.mainController = variable.controllerInstance; }

    /*
     * getProgressValues sets the level of the Progress bar when sleep game is called
     */
    public void getProgressValues() {
        this.happinessBar.setProgress(this.model.getHappiness());
        this.energyBar.setProgress(this.model.getEnergy());
        this.moneyBar.setProgress(this.model.getMoney());
        this.daysUntilEndOfTermLabel.setText("Days until end of term: " + Integer.toString(this.model.getDaysUntilEndOfTerm()));

    }

    /**setUpAnimationTimer():
     * This method sets up the timer that will act as a spinner that will cycle through the random
     * list of integers from 0-9 the hours of sleep generated in SleepGame and stored in SleepList
     */
    private void setUpAnimationTimer() {
        TimerTask timerTask = new TimerTask() {
            public void run() {
                Platform.runLater(new Runnable() {
                    public void run() {
                        updateAnimation();
                    }
                });
            }
        };

        final long startTimeInMilliseconds = 0;
        final long repetitionPeriodInMilliseconds = 1000;
        long frameTimeInMilliseconds = (long)(1000.0 / FRAMES_PER_SECOND);
        this.timer = new java.util.Timer();
        this.timer.schedule(timerTask, 0, frameTimeInMilliseconds);

    }

    /*
     * updateAnimation is responsible for changing the number displayed in the window.
     */
    public void updateAnimation() {
        sleepGameText.setText(Integer.toString(sleepList.get(sleepIndex)));
        sleepGameText.setFont(new Font(80.0));

        if(sleepIndex == 9) {
            sleepIndex = 0;
        }
        sleepIndex ++;
    }

    /*
     * sleepPushStart initializes the timer which begins the spinner on screen. The parameter startPushed
     * makes it so only one timer can be initialized.
     */
    @FXML
    protected void sleepPushStart() {
        if(!startPushed) {
            startPushed = true;
            this.setUpAnimationTimer();
        }
    }

    /*
     * sleepPushStop cancels the current timer.
     */
    @FXML
    protected void sleepPushStop() {
        this.timer.cancel();

    }

    /*
     * goBackToMainScene. This is responsible for updating the parameters in the model based on the result of SleepGame
     * as well as return back to the main screen of CarletonTrail.
     */
    @FXML
    protected void onBackButton() {
        if (startPushed == true) {
            String sleep = this.sleepGameText.getText();
            Float hours = new Float(sleep);
            Float energy = 0.1F * hours;
            Float happiness = 0.025F * hours;

            // update the energy and happiness based on how many hours of "sleep"you got
            model.updateEnergy(energy);
            model.updateHappiness(happiness);
        }

        // Remove days till end of term
        this.model.nextDay();
//        this.daysUntilEndOfTermLabel.setText("Days until end of term: " + Integer.toString(this.model.getDaysUntilEndOfTerm()));



//        this.energyBar.setProgress(model.getEnergy());

        //update the parameters in the main screen
        this.mainController.updateInformation();

        //Change the scene back to the main scene
        Scene scene = energyLabel.getScene();
        scene.setRoot(rootToReturnTo);
    }

}
