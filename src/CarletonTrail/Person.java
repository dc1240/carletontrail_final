/**
 * Person.java
 *
 * Created by Camille Benson, Dane Christensen, Chielon S. Yang
 *
 * A model for the person playing that contains information about happiness, energy, money,
 * and days until the end of term. Also contains methods for updating those values.
 */

package CarletonTrail;

public class Person implements java.io.Serializable {
    private float energy;
    private float happiness;
    private float money;
    private int grade;
    private int daysUntilEndOfTerm;

    private Person() {
    }

    public Person(float energy, float happiness, float money, int grade) {
        this.energy = energy;
        this.happiness = happiness;
        this.money = money;
        this.grade = grade;
        this.daysUntilEndOfTerm = 20;
    }

    public float getEnergy() { return this.energy; }

    public float getHappiness() { return this.happiness; }

    public float getMoney() {
        return this.money;
    }

    public int getGrade() {
        return this.grade;
    }

    public int getDaysUntilEndOfTerm() {return this.daysUntilEndOfTerm; }

    /* Update Energy based on amount given
     * @Param  float  amount in which the instance variable Grade is affected
     */
    public void updateEnergy(float change) {
        this.energy = this.energy + change;
        if (this.energy >= 1.0F) {
            this.energy = 1.0F;
        } else if (this.energy <= 0.0F) {
            this.energy = 0.0F;
        }
    }

    /* Update Happiness based on amount given
     * @Param  float  amount in which the instance variable Happiness is affected
     */
    public void updateHappiness(float change) {
        this.happiness = this.happiness + change;
        if (this.happiness >= 1.0F) {
            this.happiness = 1.0F;
        } else if (this.happiness <= 0.0F) {
            this.happiness = 0.0F;
        }
        System.out.println(this.happiness);
    }

    /* Update Money based on amount given
     * @Param  float  amount in which the instance variable Money is affected
     */
    public void updateMoney(float change) {
        this.money = this.money + change;
        if (this.money >= 1.0F) {
            this.money = 1.0F;
        } else if (this.money <= 0.0F) {
            this.money = 0.0F;
        }
    }
    /* Update Energy to be the amount given
     * @Param  int  amount in which the instance variable Grade is affected
     */
    public void updateGrade(int grade) {
        this.grade = grade;
        if (grade < 0) {
            this.grade = 0;
        } else if (grade > 100) {
            this.grade = 100;
        }
    }

    public int updateDaysUntilEndOfTerm() {
        this.daysUntilEndOfTerm = this.daysUntilEndOfTerm -1;
        return this.daysUntilEndOfTerm;
    }
}
