package CarletonTrail;

/**
 * Created by christed on 11/12/14.
 * Sleep Game will generate a group of random numbers betweeen 0-9 and cycle through it
 * allowing the user to push stop and start to determine the number of hours of sleep,
 * inturn the amount of energy that they will get from "sleeping".
 */

import java.util.ArrayList;
import java.util.Random;

public class SleepGame {
    private int hoursOfSleep;
    public ArrayList<Integer> sleepList;

    /*
     * Generates the randomly ordered list of integers from 0-9.
     */
    public SleepGame() {
        sleepList = new ArrayList<Integer>();

        Random randomNumberGenerator = new Random();

        while (sleepList.size() < 10) {
            int x = randomNumberGenerator.nextInt(10);
            if (sleepList.contains(x)) {
                continue;
            } else {
                sleepList.add(x);
            }

        }
    }

    /*
     * getSleepInts returns the list of random integers.
     */
    public ArrayList<Integer> getSleepInts() {
        return this.sleepList;
    }



    // test the get random hours of sleep list. This provides no functional benefit to the working game
    public static void main(String[] args) {
        SleepGame test = new SleepGame();

        for(int j = 0; j < test.sleepList.size(); j++){
            System.out.println("The " + j + "th number is " +  test.sleepList.get(j) );
        }
    }
}

