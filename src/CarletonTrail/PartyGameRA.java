package CarletonTrail;

/** PartyGameRA.java
 * Created by bensonc on 11/24/14.
 */


import javafx.fxml.FXML;
import javafx.scene.shape.Circle;
import javafx.scene.paint.Color;


public class PartyGameRA extends Circle {
    @FXML private double velocityX;
    @FXML private double velocityY;

    public PartyGameRA() {
//        fx:id="RA" layoutX="315" layoutY="335" radius="25" fill="red"
    }

    public void step() {
        this.setCenterX(this.getCenterX() + this.velocityX);
        this.setCenterY(this.getCenterY() + this.velocityY);
    }

    //public double getPosition() { return this.getLayoutX(); }

    public double getVelocityX() {
        return velocityX;
    }

    public void setVelocityX(double velocityX) {
        this.velocityX = velocityX;
    }

    public double getVelocityY() {
        return velocityY;
    }

    public void setVelocityY(double velocityY) {
        this.velocityY = velocityY;
    }
}

