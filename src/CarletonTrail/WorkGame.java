/**
 * WorkGame.java
 *
 * Created by Camille Benson, Dane Christensen, Chielon S. Yang
 *
 * A model for the work game.
 */

package CarletonTrail;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

import java.util.ArrayList;
import java.util.Random;

public class WorkGame extends WorkGameController{
    public AnchorPane rootToReturnTo;
    public CarletonTrailModel model;
    public Controller mainController;

    final private double FRAMES_PER_SECOND = 20.0;
    public ArrayList<Money> moneyList = new ArrayList<Money>();
    public int score;

    @FXML public Wallet wallet;
    @FXML public Money money;
    @FXML public Money money2;
    @FXML public Money money3;
    @FXML public Money money4;
    @FXML public Money money5;
    @FXML public Money money6;
    @FXML public Money money7;
    @FXML public AnchorPane gameBoard;
    @FXML public Label scoreLabel;
    @FXML public Label gameDescription;

    public WorkGame() {
        addAllMoneyToList();
        setRandomMoneyVelocity();
        setRandomMoneyPosition();
        this.score = 0;
    }

    public void addAllMoneyToList() {
        this.moneyList.add(this.money);
        this.moneyList.add(this.money2);
        this.moneyList.add(this.money3);
        this.moneyList.add(this.money4);
        this.moneyList.add(this.money5);
        this.moneyList.add(this.money6);
        this.moneyList.add(this.money7);
    }

    public void setRandomMoneyPosition() {
        for (Money m : moneyList) {
            int xPosition = randInt(100, 630);
            m.setXPosition(xPosition);
        }
    }

    /**
     * Returns a random number between min and max, inclusive.
     *
     * @param min Minimim value
     * @param max Maximim value.  Must be greater than min.
     * @return Integer between min and max, inclusive.
     */
    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public void setRandomMoneyVelocity() {
        for (Money m : moneyList) {
            int velocity = randInt(5, 15);
            m.setVelocity((double)velocity);
        }
    }

    public void getScore() {
        this.scoreLabel.setText(String.format("Score: %d", this.score));
    }

    public void setScore(int newScore) {
        this.score = newScore;
    }


}