/**
 * WorkGameController.java
 *
 * Created by Camille Benson, Dane Christensen, Chielon S. Yang
 *
 * Controller responsible for managing the work game.
 */

package CarletonTrail;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class WorkGameController extends Controller implements EventHandler<KeyEvent> {
    private AnchorPane rootToReturnTo;
    private CarletonTrailModel model;
    private Controller mainController;

    final private double FRAMES_PER_SECOND = 20.0;
    private ArrayList<Money> moneyList = new ArrayList<Money>();
    private int score;
    private Timer timer;

    @FXML private Wallet wallet;
    @FXML private Money money;
    @FXML private Money money2;
    @FXML private Money money3;
    @FXML private Money money4;
    @FXML private Money money5;
    @FXML private Money money6;
    @FXML private Money money7;
    @FXML private AnchorPane gameBoard;
    @FXML private Label scoreLabel;
    @FXML private Label gameDescription;

    public WorkGameController() {
        this.score = 0;
    }

    public void initialize() {
        this.energyLabel.setText(String.format("Energy:"));
        this.happinessLabel.setText(String.format("Happiness:"));
        this.moneyLabel.setText(String.format("Money:"));
        this.wallet.setPosition(300.0);
        showDescription();
        addAllMoneyToList();
        setRandomMoneyVelocity();
        setRandomMoneyPosition();
    }

    /**
     * Sets up a timer that steps the Money objects across the screen and displays the score.
     * It then uses updateGameAnimation() to run the game and handle whether the wallet catches
     * money object or not and updates the score.
     */
    private void setUpAnimationTimer() {
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        updateGameAnimation();
                    }
                });
            }
        };

        long frameTimeInMilliseconds = (long)(1000.0 / FRAMES_PER_SECOND);
        this.timer = new java.util.Timer();
        this.timer.schedule(timerTask, 0, frameTimeInMilliseconds);
    }

    public void updateGameAnimation() {
        getScore();
        deleteDescription();
        for (Money m : moneyList) {
            if (m.getBoundsInParent().intersects(this.wallet.getBoundsInParent()) && m.getFill() == Color.GREEN) {
                m.setFill(Color.WHITE);
                this.score += 10;
                m.step();
            } else {
                m.step();
            }
        }
    }

    public void setRootToReturnTo(AnchorPane rootToReturnTo) {
        this.rootToReturnTo = rootToReturnTo;
    }

    public void setModel(CarletonTrailModel model) {
        this.model = model;
    }

    public void setController(Controller newController) {
        this.mainController = newController.controllerInstance;
    }

    public void getProgressValues() {
        this.happinessBar.setProgress(this.model.getHappiness());
        this.energyBar.setProgress(this.model.getEnergy());
        this.moneyBar.setProgress(this.model.getMoney());
        this.daysUntilEndOfTermLabel.setText("Days until end of term: " + Integer.toString(this.model.getDaysUntilEndOfTerm()));
    }

    public void addAllMoneyToList() {
        this.moneyList.add(this.money);
        this.moneyList.add(this.money2);
        this.moneyList.add(this.money3);
        this.moneyList.add(this.money4);
        this.moneyList.add(this.money5);
        this.moneyList.add(this.money6);
        this.moneyList.add(this.money7);
    }

    public void setRandomMoneyPosition() {
        for (Money m : moneyList) {
            int xPosition = randInt(100, 630);
            m.setXPosition(xPosition);
        }
    }

    public void setRandomMoneyVelocity() {
        for (Money m : moneyList) {
            int velocity = randInt(5, 15);
            m.setVelocity((double)velocity);
        }
    }

    /**
     * Returns a random number between a min and max value, inclusive.
     *
     * @param min Minimim value
     * @param max Maximim value.  Must be greater than min.
     * @return Integer between min and max, inclusive.
     */
    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    private void getScore() {
        this.scoreLabel.setText(String.format("Score: %d", this.score));
    }

    private void showDescription() {
        this.gameDescription.setText("Press the left and right arrow keys to move the wallet back and \n forth and try to catch as much cash as possible. Good luck!");
    }

    private void deleteDescription() {
        this.gameDescription.setText("");
    }

    /**
     * A button that returns you to the main screen and updates all the information for the model
     * based on how you did in this game.
     *
     * @param actionEvent
     * @throws Exception
     */
    @FXML
    public void onWorkButton(ActionEvent actionEvent) throws Exception {
        this.model.nextDay();
        this.daysUntilEndOfTermLabel.setText("Days until end of term: " + Integer.toString(this.model.getDaysUntilEndOfTerm()));
        model.updateMoney(this.score / 100.0F);
        this.mainController.updateInformation();
        Scene scene = energyLabel.getScene();
        scene.setRoot(rootToReturnTo);
    }

    /**
     * Begins the animation timer after pressing the the Start button.
     *
     * @param actionEvent
     * @throws Exception
     */
    @FXML
    public void onStartButton(ActionEvent actionEvent) throws Exception {
        this.setUpAnimationTimer();
    }

    /**
     * Handles key strokes to move the wallet object. Implemented through Controller.java
     * by setting up a KeyEvent handler and putting focus on the root node after starting up
     * and instance of the work game.
     *
     * @param keyEvent
     */
    @Override
    public void handle(KeyEvent keyEvent) {
        KeyCode keyCode = keyEvent.getCode();
        if (keyCode == KeyCode.LEFT) {
            if (this.wallet.getPosition() > 100.0) {
                this.wallet.stepLeft();
                keyEvent.consume();
            }
        } else if (keyCode == KeyCode.RIGHT) {
            if (this.wallet.getPosition() < 600.0) {
                this.wallet.stepRight();
                keyEvent.consume();
            }
        }
    }
}