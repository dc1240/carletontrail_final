/**
 * Controller.java
 *
 * Created by Camille Benson, Dane Christensen, Chielon S. Yang
 *
 * This controller is responsible for switching between the main scene and the scene for each game while keeping track
 * of and updating the progress bars and days until the end of term.
 */

package CarletonTrail;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class Controller {

    public Label energyLabel;
    public Label happinessLabel;
    public Label moneyLabel;
    public Label gradeLabel;
    public Label daysUntilEndOfTermLabel;
    public ImageView carletonSigill;
    public ProgressBar happinessBar;
    public ProgressBar energyBar;
    public ProgressBar moneyBar;
    public CarletonTrailModel model;
    public Controller controllerInstance;

    public Controller() {}

    /* Sets the text displays from the FXML file and displays on the screen.
     * Loads image of the Carleton sigill onto screen.
     */
    public void initialize() {
        // Update information
        this.model = new CarletonTrailModel();
        this.energyLabel.setText(String.format("Energy:"));
        this.happinessLabel.setText(String.format("Happiness:"));
        this.moneyLabel.setText(String.format("Money:"));
        this.happinessBar.setProgress(model.getHappiness());
        this.energyBar.setProgress(model.getEnergy());
        this.moneyBar.setProgress(model.getMoney());
        this.daysUntilEndOfTermLabel.setText("Days until end of term: " + Integer.toString(this.model.getDaysUntilEndOfTerm()));
        this.gradeLabel.setText("Grade: " + this.model.getLetterGrade());

        // Display Carleton sigill
        Image image = new Image(getClass().getResourceAsStream("Carletoncollegeseal.jpeg"));
        carletonSigill.setImage(image);
    }

    /* onStudyButton switches to the study game scene once the user presses
     * the Study button. Also updates all status categories.
     */
    @FXML
    protected void onStudyButton(ActionEvent actionEvent) throws Exception {
        // Update information
        model.updateHappiness(-0.1F);
        model.updateEnergy(-0.05F);
        this.happinessBar.setProgress(model.getHappiness());
        this.energyBar.setProgress(model.getEnergy());
        this.moneyBar.setProgress(model.getMoney());
        this.daysUntilEndOfTermLabel.setText("Days until end of term: " + Integer.toString(this.model.getDaysUntilEndOfTerm()));
        this.gradeLabel.setText("Grade: " + this.model.getLetterGrade());

        // Load the StudyGame fxml and Controller
        FXMLLoader loader = new FXMLLoader(getClass().getResource("StudyGame.fxml"));
        Parent root = loader.load();
        StudyGameController controller = (StudyGameController)loader.getController();

        // Provide information to route back to after individual game is done
        Scene scene = this.energyLabel.getScene();
        controller.setRootToReturnTo((AnchorPane)scene.getRoot());
        controller.setModel(this.model);
        controller.setController(this.controllerInstance);
        controller.getProgressValues();

        // Get key inputs to work
        root.setOnKeyPressed(controller);
        root.requestFocus();

        // Set the new scene
        scene.setRoot(root);
    }

    @FXML
    protected void onPartyButton(ActionEvent actionEvent) throws Exception {
        // Update information
        model.updateHappiness(0.3F);
        model.updateEnergy(-0.3F);
        model.updateMoney(-0.3F);
        model.updateGrade(model.getGrade()-20);
        this.happinessBar.setProgress(model.getHappiness());
        this.energyBar.setProgress(model.getEnergy());
        this.moneyBar.setProgress(model.getMoney());
        this.daysUntilEndOfTermLabel.setText("Days until end of term: " + Integer.toString(this.model.getDaysUntilEndOfTerm()));
        this.gradeLabel.setText("Grade: " + this.model.getLetterGrade());


        // Load the StudyGame fxml and Controller
        FXMLLoader loader = new FXMLLoader(getClass().getResource("PartyGame.fxml"));
        Parent root = loader.load();
        PartyGameController controller = (PartyGameController)loader.getController();
        Scene scene = this.energyLabel.getScene();

        // Provide information to route back to after individual game is done
        controller.setRootToReturnTo((AnchorPane)scene.getRoot());
        controller.setModel(this.model);
        controller.setController(this.controllerInstance);
        controller.getProgressValues();

        // Get key inputs to work
        root.setOnKeyPressed(controller);
        root.requestFocus();

        // Set the new scene
        scene.setRoot(root);
    }

    @FXML
    protected void onSleepButton(ActionEvent actionEvent) throws Exception {
        System.out.println("Study Now!");
        model.updateGrade(model.getGrade()-5);
        this.happinessBar.setProgress(model.getHappiness());
        this.energyBar.setProgress(model.getEnergy());
        this.moneyBar.setProgress(model.getMoney());
        this.daysUntilEndOfTermLabel.setText("Days until end of term: " + Integer.toString(this.model.getDaysUntilEndOfTerm()));
        this.gradeLabel.setText("Grade: " + this.model.getLetterGrade());

        // Load the StudyGame fxml and Controller
        FXMLLoader loader = new FXMLLoader(getClass().getResource("SleepGame.fxml"));
        Parent root = loader.load();
        SleepGameController sleepController = (SleepGameController)loader.getController();
        Scene scene = this.energyLabel.getScene();

        // Provide information to route back to after individual game is done
        sleepController.setRootToReturnTo((AnchorPane)scene.getRoot());
        sleepController.setModel(this.model);
        sleepController.setController(this.controllerInstance);
        sleepController.getProgressValues();

        // Set the new scene
        scene.setRoot(root);


    }

    @FXML
    protected void onWorkButton(ActionEvent actionEvent) throws Exception {
        model.updateHappiness(-0.1F);
        model.updateEnergy(-0.1F);
        model.updateMoney(0.1F);
        this.happinessBar.setProgress(model.getHappiness());
        this.energyBar.setProgress(model.getEnergy());
        this.moneyBar.setProgress(model.getMoney());
        this.daysUntilEndOfTermLabel.setText("Days until end of term: " + Integer.toString(this.model.getDaysUntilEndOfTerm()));
        this.gradeLabel.setText("Grade: " + this.model.getLetterGrade());

        // Load the StudyGame fxml and Controller
        FXMLLoader loader = new FXMLLoader(getClass().getResource("WorkGame.fxml"));
        Parent root = loader.load();
        WorkGameController controller = (WorkGameController)loader.getController();
        Scene scene = this.energyLabel.getScene();
        controller.setRootToReturnTo((AnchorPane)scene.getRoot());
        controller.setModel(this.model);
        controller.setController(this.controllerInstance);
        controller.getProgressValues();

        // Get key inputs to work
        root.setOnKeyPressed(controller);
        root.requestFocus();

        // Set the new scene
        scene.setRoot(root);
    }


    public void updateInformation() {
        this.happinessBar.setProgress(model.getHappiness());
        this.energyBar.setProgress(model.getEnergy());
        this.moneyBar.setProgress(model.getMoney());
        this.daysUntilEndOfTermLabel.setText("Days until end of term: " + Integer.toString(this.model.getDaysUntilEndOfTerm()));
        this.gradeLabel.setText("Grade: " + this.model.getLetterGrade());
        
        if(this.model.getDaysUntilEndOfTerm() == 0){
            System.exit(0);
        }
    }

    public void setControllerInstance(Controller controller) {
        this.controllerInstance = controller;
    }


}
