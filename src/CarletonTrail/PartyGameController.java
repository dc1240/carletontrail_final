package CarletonTrail;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Written by Camille on 11/21/14.
 */
public class PartyGameController extends Controller implements EventHandler<KeyEvent> {
    private AnchorPane rootToReturnTo;
    private CarletonTrailModel model;
    private Controller mainController;
    private double FRAMES_PER_SECOND = 1.0;
    private Timer timer;
    private boolean START = false;
    private boolean has_beer = false;
    private double RAVelocity;
    private int SCORE = 0;
    private int beer = 5;


    @FXML private Text directions;
    @FXML private Text directions_2;
    @FXML private Label beers_left;
    @FXML private Label score;
    @FXML private PartyGameRA RA;
    @FXML private Circle player;

    public PartyGameController() {
    }

    public void initialize() {
        this.energyLabel.setText(String.format("Energy:"));
        this.happinessLabel.setText(String.format("Happiness:"));
        this.moneyLabel.setText(String.format("Money:"));
        this.beers_left.setText("5");
        this.beers_left.setFont(new Font(30.0));
        this.directions.setText("Get 5 beers from your friends room using the arrow keys to move and ENTER key ");
        this.directions_2.setText("to pick up and drop off beer. Don't let the RA catch you!");
        this.score.setFont(new Font(50.0));
        this.directions.setFont(new Font(18.0));
        this.directions_2.setFont(new Font(18.0));

    }

    public void setRootToReturnTo(AnchorPane rootToReturnTo) {
        this.rootToReturnTo = rootToReturnTo;
    }
    public void setModel(CarletonTrailModel model) {
        this.model = model;
    }
    /*
   * setController. setController allows us to pass in an instance of the main Controller. By doing this we are able to update
   * the values on the CarletonTrail.fxml file after the game is complete.
   */
    public void setController(Controller variable) { this.mainController = variable.controllerInstance; }

    public void getProgressValues() {
        this.happinessBar.setProgress(this.model.getHappiness());
        this.energyBar.setProgress(this.model.getEnergy());
        this.moneyBar.setProgress(this.model.getMoney());
        this.daysUntilEndOfTermLabel.setText("Days Left: " + Integer.toString(this.model.getDaysUntilEndOfTerm()));
        this.gradeLabel.setText("Grade: " + this.model.getLetterGrade());

    }


    // Returns user to main controller and main page when Back button pressed
    @FXML
    public void onBackButton(ActionEvent actionEvent) throws Exception {
        this.model.nextDay();
        this.daysUntilEndOfTermLabel.setText("Days until end of term: " + Integer.toString(this.model.getDaysUntilEndOfTerm()));

        Scene scene = energyLabel.getScene();
        scene.setRoot(rootToReturnTo);
    }

    /* Begins the AnimationTimer to begin the game
     * once user clicks the START button.
     */
    @FXML
    public void onStartButton(ActionEvent actionEvent) throws Exception {
        if(START == false) {
            START = true;
            setUpAnimationTimer();
        }
    }

    private void setUpAnimationTimer() {
        TimerTask timerTask = new TimerTask() {
            public void run() {
                Platform.runLater(new Runnable() {
                    public void run() {
                        updateAnimation();
                    }
                });
            }
        };

        long frameTimeInMilliseconds = (long)(1000.0 / FRAMES_PER_SECOND);
        this.timer = new java.util.Timer();
        this.timer.schedule(timerTask, 0, frameTimeInMilliseconds);
    }
    // updates the animation and moves the RA accordingly
    public void updateAnimation() {
        RACatchPlayer();
        double RACenterX = this.RA.getCenterX() + this.RA.getLayoutX();
        double RARadius = this.RA.getRadius();
        RAVelocity = RA.getVelocityX();

        beers_left.setText(Integer.toString(beer));

        if (RACenterX + RARadius > 800 && RAVelocity > 0) {
            this.RA.setCenterX(400);
            this.RA.setVelocityX(-RAVelocity);
        } else if (RACenterX - RARadius < 0 && RAVelocity < 0) {
            this.RA.setCenterX(-300);
            this.RA.setVelocityX(-RAVelocity);
        }
        RA.step();

    }

    // Checks if RA and Player intersect on the screen
    // If they interact, the player starts back at 0
    public void RACatchPlayer(){
        //if RA and Player interact:
        if (RA.getBoundsInParent().intersects(player.getBoundsInParent())) {
            SCORE = 0;
            score.setText("0");
            beer = 5;
            has_beer = false;
            beers_left.setText("5");
            player.setLayoutX(130);
            player.setLayoutY(180);
        }
    }

    /** Checks where the player is located on the screen
     * provides information to set boundaries.
     * @return position of player
     */
    public String checkPosition() {
        String position = "";
        double X = this.player.getLayoutX();
        double Y = this.player.getLayoutY();

        // Hard ints are boundaries based on the "rooms" and "hallway" shapes provided in screen
        if (X >= 80 && X <= 300 && Y >= 110 && Y < 300) {
            position = "dorm room";
        } else if (X >= 0 && X <= 800 && Y >= 300 && Y <= 380) {
            position = "hallway";
        } else if (X >= 450 && X <= 690 && Y >= 110 && Y < 300) {
            position = "other room";
        }
        return position;
    }

    /** Will move player and ensures the player does not go off the game board.
     *
     * @param keyEvent
     */
    @Override
    public void handle(KeyEvent keyEvent) {
        KeyCode code = keyEvent.getCode();
        double stepSize = 15.0;

        if (code == KeyCode.RIGHT) {
            if (checkPosition().equals("dorm room")) {
                this.player.setLayoutX(this.player.getLayoutX() + stepSize);
                if (this.player.getLayoutX() > 300) {
                    this.player.setLayoutX(300);
                }
            } else if (checkPosition().equals("hallway")) {
                this.player.setLayoutX(this.player.getLayoutX() + stepSize);
                if (this.player.getLayoutX() > 790) {
                    this.player.setLayoutX(790);
                }
            } else if (checkPosition().equals("other room")) {
                this.player.setLayoutX(this.player.getLayoutX() + stepSize);
                if (this.player.getLayoutX() > 670) {
                    this.player.setLayoutX(670);
                }
            }
            keyEvent.consume();
        } else if (code == KeyCode.LEFT) {
            if (checkPosition().equals("dorm room")) {
                this.player.setLayoutX(this.player.getLayoutX() - stepSize);
                if (this.player.getLayoutX() < 100) {
                    this.player.setLayoutX(100);
                }
            } else if (checkPosition().equals("hallway")) {
                this.player.setLayoutX(this.player.getLayoutX() - stepSize);
                if (this.player.getLayoutX() < 10) {
                    this.player.setLayoutX(10);
                }
            } else if (checkPosition().equals("other room")) {
                this.player.setLayoutX(this.player.getLayoutX() - stepSize);
                if (this.player.getLayoutX() < 468) {
                    this.player.setLayoutX(468);
                }
            }
            keyEvent.consume();
        } else if (code == KeyCode.UP) {
            if (checkPosition().equals("dorm room")) {
                this.player.setLayoutY(this.player.getLayoutY() - stepSize);
                if (this.player.getLayoutY() < 140) {
                    this.player.setLayoutY(140);
                }
            } else if (checkPosition().equals("hallway")) {
                this.player.setLayoutY(this.player.getLayoutY() - stepSize);
                if (this.player.getLayoutY() < 320) {
                    if (this.player.getLayoutX() < 80 || this.player.getLayoutX() > 690) {
                        this.player.setLayoutY(320);
                    } if (this.player.getLayoutX() < 450 && this.player.getLayoutX() > 300) {
                        this.player.setLayoutY(320);
                    }
                }
            } else if (checkPosition().equals("other room")) {
                this.player.setLayoutY(this.player.getLayoutY() - stepSize);
                if (this.player.getLayoutY() < 140) {
                    this.player.setLayoutY(140);
                }
            }
            keyEvent.consume();
        } else if (code == KeyCode.DOWN) {
            this.player.setLayoutY(this.player.getLayoutY() + stepSize);
            if (checkPosition().equals("hallway")) {
                if (this.player.getLayoutY() > 360) {
                    this.player.setLayoutY(360);
                }
            }
            keyEvent.consume();
        } else if (code == KeyCode.ENTER) {
            if (checkPosition().equals("other room") && has_beer == false) {
                if (beer != 0) { beer--; }
                this.has_beer = true;
            } else if (checkPosition().equals("dorm room") && has_beer == true) {
                this.SCORE++;
                score.setText(Integer.toString(SCORE));
                this.has_beer = false;
            }
            keyEvent.consume();
        }

    }
}
