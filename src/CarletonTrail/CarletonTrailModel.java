/**
 * CarletonTrailModel.java
 *
 * Created Camille Benson, Dane Christensen, Chielon S. Yang
 *
 * The main model that Creates a Person/Player object. Interacts with Main Controller
 * to provide appropriate information of the player.
 */

package CarletonTrail;

import java.io.*;

public class CarletonTrailModel {
    private Person user;

    public CarletonTrailModel() {
        this.user = new Person(1.0F, 0.5F, 0.5F, 100);
    }

    /* Save and Load functions attempt -- ran out of time to continue implementing.
     *
     */
//    public void save() {
//        Person person = new Person(1.0F, 0.5F, 0.5F, 100);
//        try {
//            FileOutputStream fileOut = new FileOutputStream("/src/person.ser");
//            ObjectOutputStream out = new ObjectOutputStream(fileOut);
//            out.writeObject(person);
//            out.close();
//            fileOut.close();
//        } catch (IOException i) {
//            i.printStackTrace();
//        }
//    }
//
//    public void load() {
//        Person person = null;
//        try {
//            FileInputStream fileIn = new FileInputStream("/src/person.ser");
//            ObjectInputStream in = new ObjectInputStream(fileIn);
//            person = (Person) in.readObject();
//            in.close();
//            fileIn.close();
//        } catch (IOException i) {
//            i.printStackTrace();
//            return;
//        } catch (ClassNotFoundException c) {
//            c.printStackTrace();
//            return;
//        }
//    }

    public float getEnergy() { return this.user.getEnergy(); }

    public float getHappiness() { return this.user.getHappiness(); }

    public float getMoney() {
        return this.user.getMoney();
    }

    public int getGrade() {
        return this.user.getGrade();
    }

    public int getDaysUntilEndOfTerm() {return this.user.getDaysUntilEndOfTerm(); }

    public void updateEnergy(float amountChanged) { this.user.updateEnergy(amountChanged); }

    public void updateHappiness(float amountChanged) { this.user.updateHappiness(amountChanged); }

    public void updateMoney(float amountChanged) { this.user.updateMoney(amountChanged); }

    public void updateGrade(int amountChanged) { this.user.updateGrade(amountChanged); }

    public void nextDay() { this.user.updateDaysUntilEndOfTerm(); }

    public String getLetterGrade() {
        String letter_grade = "";
        double percentage = (double) this.user.getGrade();
        if (percentage > 89) { letter_grade = "A"; }
        else if (percentage > 79) { letter_grade = "B"; }
        else if (percentage > 69) { letter_grade = "C"; }
        else if (percentage > 59) { letter_grade = "D"; }
        else if (percentage <= 59) { letter_grade = "F"; }
        return letter_grade;
    }

}
