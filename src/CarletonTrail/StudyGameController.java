package CarletonTrail;

import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.Label;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import java.util.*;
import java.util.Timer;

/** A separate controller for the Study Game.
 *  Created by Camille on 11/21/14.
 */
public class StudyGameController extends Controller implements EventHandler<KeyEvent> {
    private AnchorPane rootToReturnTo;
    private CarletonTrailModel model;
    private Controller mainController;
    private StudyGame game;
    private Timer timer;
    private ArrayList<Integer> timerList;
    private int timerIndex = 0;
    private String assigned_shape;
    private boolean START = false;
    private boolean key_pressed = true;
    private double FRAMES_PER_SECOND = 1.0;
    private int TIME_ALLOWED = 20;
    private int MOVE_OUT_OF_WINDOW = 5000;

    @FXML private Text timerLabel;
    @FXML private Text directions;
    @FXML private Label score;
    @FXML private Text P;
    @FXML private Text Q;
    @FXML private Polygon triangle;
    @FXML private Rectangle square;

    public StudyGameController() {
        game = new StudyGame();
        timerList = game.createTimerList(TIME_ALLOWED);
    }

    public void initialize() {
        this.energyLabel.setText(String.format("Energy:"));
        this.happinessLabel.setText(String.format("Happiness:"));
        this.moneyLabel.setText(String.format("Money:"));

        this.directions.setText(String.format("You have %d seconds to press the appropriate keys (P or Q) when either shape pops up.", TIME_ALLOWED));
        P.setFont(new Font(30.0));
        Q.setFont(new Font(30.0));
        score.setFont(new Font(30.0));
    }

    public void setRootToReturnTo(AnchorPane rootToReturnTo) { this.rootToReturnTo = rootToReturnTo; }

    public void setModel(CarletonTrailModel model) { this.model = model; }

    /*
   * setController. setController allows us to pass in an instance of the main Controller. By doing this we are able to update
   * the values on the CarletonTrail.fxml file after the game is complete.
   */
    public void setController(Controller variable) { this.mainController = variable.controllerInstance; }

    /* Set the progress bars to the current values.
     * Used by the Main Controller to provide the progress information when switching scenes.
     */
    public void getProgressValues() {
        this.happinessBar.setProgress(this.model.getHappiness());
        this.energyBar.setProgress(this.model.getEnergy());
        this.moneyBar.setProgress(this.model.getMoney());
        this.daysUntilEndOfTermLabel.setText("Days Left: " + Integer.toString(this.model.getDaysUntilEndOfTerm()));
        this.gradeLabel.setText("Grade: " + this.model.getLetterGrade());
        this.score.setText("Score: 0");
    }

    /* Returns to the Main Scene and Main Controller
     * after taking off a day from total number days left in the term.
     */
    @FXML
    public void onBackButton(ActionEvent actionEvent) throws Exception {
        START = false;
        this.model.nextDay();
        this.daysUntilEndOfTermLabel.setText("Days Left: " + Integer.toString(this.model.getDaysUntilEndOfTerm()));
        Scene scene = energyLabel.getScene();
        scene.setRoot(rootToReturnTo);
    }

    /* Begins the AnimationTimer to begin the game
     * once user clicks the START button.
     */
    @FXML
    public void onStartButton(ActionEvent actionEvent) throws Exception {
        if(START == false) {
            START = true;
            setUpAnimationTimer();
        }
    }

    /* Creates a new TimerTask and updates the animation accordingly.
     * Resets the size of the timerLabel to a larger size.
     */
    private void setUpAnimationTimer() {
        TimerTask timerTask = new TimerTask() {
            public void run() {
                Platform.runLater(new Runnable() {
                    public void run() {
                        updateAnimation();
                    }
                });
            }
        };

        long frameTimeInMilliseconds = (long) (1000.0 / FRAMES_PER_SECOND);
        this.timer = new java.util.Timer();
        this.timer.schedule(timerTask, 0, frameTimeInMilliseconds);
        timerLabel.setFont(new Font(40.0));
    }

    /* Sets the timerLabel to the countdown list.
     * Controls the appearance of the triangle or the square.
     * Returns to Main Page and Main Controller after time is up.
     */
    public void updateAnimation() {
        timerLabel.setText(Integer.toString(timerList.get(timerIndex)));

        // Sees whether a user has pressed a key once the animation is called to be updated
        if (key_pressed == true) {
            // Chooses a shape and determines which one is moved out of the view
            // and which one is shown
            assigned_shape = game.assignShape();
            if (assigned_shape == "triangle") {
                if (square.getX() < MOVE_OUT_OF_WINDOW) {
                    square.setTranslateX(MOVE_OUT_OF_WINDOW);
                }
                triangle.setTranslateX(0);
            } else {
                if (triangle.getLayoutX() < MOVE_OUT_OF_WINDOW) {
                    triangle.setTranslateX(MOVE_OUT_OF_WINDOW);
                }
                square.setTranslateX(0);
            }
            key_pressed = false;
        }

        // Once timer has reached the end, Calculates the score and updates the model's grade.
        // Returns to Main Page as well as the Main Controller.
        if(timerIndex == TIME_ALLOWED) {
            this.timer.cancel();
//            System.out.print("WHEN DONE");
//            System.out.println(game.reportScore());
            START = false;
            float score = game.reportScore() / (float) TIME_ALLOWED;
            score = score * 100;
//            System.out.print("FINAL");
            System.out.println(score);
            this.model.updateGrade((int) score);
            this.model.nextDay();
            this.daysUntilEndOfTermLabel.setText("Days Left: " + Integer.toString(this.model.getDaysUntilEndOfTerm()));
            this.gradeLabel.setText("Grade: " + this.model.getLetterGrade());
            System.out.print(this.model.getLetterGrade());
            this.mainController.updateInformation();
            Scene scene = energyLabel.getScene();
            scene.setRoot(rootToReturnTo);
        }
        timerIndex ++;

    }

    /* Handles the event of P or Q being pressed.
     * Adds or subtracts one to the score depending if the key pressed
     * was the correct one.
     */
    @Override
    public void handle(KeyEvent keyEvent) {
        KeyCode code = keyEvent.getCode();

        if (code == KeyCode.P) {
            // Check if key pressed
            if (assigned_shape == "square") {
                game.scoreUpdate(1);
                square.setTranslateX(5000);
                this.score.setText("Score: " + Integer.toString(game.reportScore()));

            } else {
                game.scoreUpdate(-1);
                triangle.setTranslateX(5000);
                this.score.setText("Score: " + Integer.toString(game.reportScore()));
            }
            key_pressed = true;
            keyEvent.consume();
        } else if (code == KeyCode.Q) {
            if (assigned_shape == "triangle") {
                game.scoreUpdate(1);
                triangle.setTranslateX(5000);
                this.score.setText("Score: " + Integer.toString(game.reportScore()));
            } else {
                game.scoreUpdate(-1);
                square.setTranslateX(5000);
                this.score.setText("Score: " + Integer.toString(game.reportScore()));
            }
            key_pressed = true;
            keyEvent.consume();
        }
    }
}


