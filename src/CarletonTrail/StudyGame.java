package CarletonTrail;

/** StudyGame
 * 1. Keeps track of total score by updating and returning the final count
 * 2. Uses random number generator to choose either triangle or square shape to be shown
 * 3. Creates a countdown list based on the input amount
 * Created by bensonc on 11/14/14.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class StudyGame {
    public int SCORE = 0;

    public StudyGame(){
    }

    /* Method generates a random number between 0 and 1
     * RETURNS int of a random number
     */
    public int randomNumberGen(){
        Random randomNumberGenerator = new Random();
        int randomNum = randomNumberGenerator.nextInt(2);
        return randomNum;
    }

    /* Method chooses the shape to be displayed based on random number
     * RETURNS String of either triangle or square
     */
    public String assignShape(){
        String shape = "square";
        if (randomNumberGen() == 1) { shape = "triangle"; }
        return shape;
    }

    /* Updates the total score
     * @PARAM int adds value to the existing score
     */
    public void scoreUpdate(int addedValue){
        this.SCORE = this.SCORE + addedValue;
        if(this.SCORE<0){
            this.SCORE = 0;
        }
    }

    // Reports the final score
    public int reportScore(){ return this.SCORE; }

    /* Creates the countdown list to be used
     * @PARAM int determines the length of the ArrayList
     */
    public ArrayList<Integer> createTimerList(int timeInSeconds){
        List list = new ArrayList<Integer>();
        for(int i=timeInSeconds; i>-1; i-- ) {
            list.add(i);
        }
        return (ArrayList<Integer>) list;
    }

}
