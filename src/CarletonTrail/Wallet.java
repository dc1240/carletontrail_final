/**
 * Wallet.java
 *
 * Created by Camille Benson, Dane Christensen, Chielon S. Yang
 *
 * Model for wallet object that sets its position and has a step function for moving it across the screen.
 */

package CarletonTrail;

import javafx.scene.shape.Rectangle;


public class Wallet extends Rectangle {
    private double position;

    public Wallet() {
    }

    public double getPosition() {
        return this.position;
    }

    public void setPosition(double newPosition) {
        this.position = newPosition;
    }

    /**
     * Move the Wallet object left and right.
     */
    public void stepLeft() {
        this.position -= 10.0;
        this.setLayoutX(this.position);
    }

    public void stepRight() {
        this.position += 10.0;
        this.setLayoutX(this.position);
    }
}
